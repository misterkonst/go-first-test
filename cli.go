package main

import (
	"bufio"
	"flag"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
)

var host string
var port int
var token string
var scope string

func init() {
	flag.StringVar(&host, "host", "127.0.0.1", "Server address param")
	flag.IntVar(&port, "port", 8091, "Server host param")
	//Не реализован (просто отправить abracadabra в командной строке
	flag.StringVar(&token, "token", "nil", "Send param")
	//так и не понял за что должен отвечать этот параметр.
	flag.StringVar(&scope, "scope", "", "Use scope param")
}

func main() {
	flag.Parse()

	conn := initialConnection()

	InitCli(conn)
}

func InitCli(conn net.Conn) {
	printMessageFromServer(conn)
	text := waitingCommand()
	firstStart := true
	for text != "exit" {
		if firstStart == false {
			text = waitingCommand()
		}
		fmt.Fprintf(conn, text+"\n")
		printMessageFromServer(conn)
		if text == "exit" {
			fmt.Print("Bye from CLI...")
			break
		}
		firstStart = false
	}
}

/*
 * Ожидание ввода команды
 */
func waitingCommand() string {
	fmt.Print("insert command: ")
	text, _ := bufio.NewReader(os.Stdin).ReadString('\n')
	text = strings.TrimSpace(text)

	return text
}

/*
 * Выводим полученные сообщения с сервера
 */
func printMessageFromServer(conn net.Conn) {
	message, err := bufio.NewReader(conn).ReadString('\n')
	if err != nil {
		fmt.Print(err)
	}
	fmt.Print("Message from server: " + message)
}

/*
 * инициализация подключения к серверу
 */
func initialConnection() net.Conn {
	conn, _ := net.Dial("tcp", host+":"+strconv.Itoa(port))

	return conn
}
