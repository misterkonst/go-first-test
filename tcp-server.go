package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"math/rand"
	"net"
	"strconv"
	"strings"
	"time"
)

var serverPort int
var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
var bearerToken string

type userStruct struct {
	UserId    string
	AuthToken string
	Status    bool
}

func init() {
	flag.IntVar(&serverPort, "port", 8091, "Use host param")
}

func main() {
	flag.Parse()

	fmt.Println("Launching server...")

	ln, err := net.Listen("tcp", ":"+strconv.Itoa(serverPort))
	fmt.Println("server listen " + strconv.Itoa(serverPort) + " port")
	if err != nil {
		fmt.Println(err)
		return
	}

	for {
		connection, err := ln.Accept()
		if err != nil {
			fmt.Println(err)
			return
		}

		go handleConnection(connection)
	}
}

/**
 * горутина для обработки запросов
 */
func handleConnection(connection net.Conn) {
	token := "abracadabra"
	connAddr := connection.RemoteAddr().String()
	fmt.Println("new connection " + connAddr)
	fmt.Println("token:" + bearerToken)
	connection.Write([]byte("Hello " + connAddr + "\n\r"))

	defer connection.Close()

	scanner := bufio.NewScanner(connection)
	for scanner.Scan() {
		text := scanner.Text()
		text = strings.TrimSpace(text)
		if text == "exit" {
			connection.Write([]byte("Come back soon... \n\r"))
			fmt.Println("Connection close " + connAddr)
			break
		} else if text == token {
			//если пришел токен abracadabra (каждый раз будет генерироваться новый токен)
			fmt.Println("Generate new bearer token")
			rand.Seed(time.Now().UnixNano())
			bearerToken = randSeq(30)
			user := userStruct{
				connAddr,
				bearerToken,
				true,
			}

			responseJson, _ := json.Marshal(user)
			fmt.Println(bearerToken)
			connection.Write([]byte(string(responseJson) + "\n\n"))
		} else if text == bearerToken {
			//обработка бирер токена ( при респонсе токен не меняется)
			user := userStruct{
				connAddr,
				bearerToken,
				true,
			}
			responseJson, _ := json.Marshal(user)
			fmt.Println(responseJson)
			connection.Write([]byte(string(responseJson) + "\n\n"))
		} else if text != "" {
			fmt.Println(bearerToken)
			connection.Write([]byte("Wrong command: " + text + ", please paste " + token + " or send you bearer token \n\r"))
		}
	}
}

/**
 * генерируем случайную строку
 */
func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
